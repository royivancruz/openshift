const express = require('express');
const app = express();

require('dotenv').config();

const bodyParser = require('body-parser');
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));

app.get('/', (req, res, next) => {
    res.setHeader("Content-Type", "application/json");

    let schema = {
        "message": "Welcome to Sample NodeJS Microservice API Resources!"
    }

    console.log(schema);
    res.send(schema);
});

app.get('/v1/ivan/test', (req, res, next) => {
    res.setHeader("Content-Type", "application/json");

    let schema = {
        "method": "GET",
        "message": "Hello Openshift from NodeJS microservice!",
        "headers": req.headers,
        "query": req.query,
        "body": req.body
    }

    console.log(schema);
    res.send(schema);
});

app.post('/v1/ivan/test', (req, res, next) => {
    res.setHeader("Content-Type", "application/json");
    let schema = {
        "method": "POST",
        "message": "Hello Openshift from NodeJS microservice!",
        "headers": req.headers,
        "query": req.query,
        "body": req.body
    }

    console.log(schema);
    res.send(schema);
});

app.put('/v1/ivan/test', (req, res, next) => {
    res.setHeader("Content-Type", "application/json");
    let schema = {
        "method": "PUT",
        "message": "Hello Openshift from NodeJS microservice!",
        "headers": req.headers,
        "query": req.query,
        "body": req.body
    }

    console.log(schema);
    res.send(schema);
});

let port = process.env.PORT || 8080;
app.listen(port, function () {
    console.log('[App] Now up and running', { port: port });
});